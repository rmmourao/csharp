﻿using System;

namespace ExercicioOrientacaoObjetos_25
{
    class Program
    {
        static void Main(string[] args)
        {
            Pessoa p1 = new Pessoa();
            Pessoa p2 = new Pessoa();
            string result;

            System.Console.Write("Informe o nome da 1ª pessoa: ");
            p1.Nome = Console.ReadLine();

            System.Console.Write("Informe a idade da 1ª pessoa: ");
            p1.Idade = int.Parse(Console.ReadLine());

            System.Console.Write("Informe o nome da 2ª pessoa: ");
            p2.Nome = Console.ReadLine();

            System.Console.Write("Informe a idade da 2ª pessoa: ");
            p2.Idade = int.Parse(Console.ReadLine());

            if (p1.Idade > p2.Idade)
            {
                result = p1.Nome;
            }
            else
            {
                result = p2.Nome;
            }

            System.Console.WriteLine("A pessoal mais velha é: " + result);
        }
    }
}

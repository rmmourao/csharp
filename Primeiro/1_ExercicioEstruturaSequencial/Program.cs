﻿using System;

namespace ExercicioEstruturaSequencial
{
    class Program
    {
        /*
            Faça um programa para ler dois valores inteiros, e depois mostrar na tela a soma desses números com uma mensagem explicativa.
        */
        static void Main(string[] args)
        {
            System.Console.WriteLine("Insira o 1º número inteiro:");
            int n1 = int.Parse(Console.ReadLine());

            System.Console.WriteLine("Insira o 2º número inteiro:");
            int n2 = int.Parse(Console.ReadLine());

            System.Console.WriteLine($"A soma entre {n1} + {n2} é {n1 + n2}.");
        }
    }
}

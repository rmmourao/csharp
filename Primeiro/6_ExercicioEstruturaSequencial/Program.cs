﻿using System;
using System.Globalization;

namespace ExercicioEstruturaSequencial
{
    class Program
    {
        /*
            Fazer um programa que leia três valores com ponto flutuante de dupla precisão: A, B e C. Em seguida, calcule e mostre:
                a) a área do triângulo retângulo que tem A por base e C por altura.
                b) a área do círculo de raio C. (pi = 3.14159)
                c) a área do trapézio que tem A e B por bases e C por altura.
                d) a área do quadrado que tem lado B.
                e) a área do retângulo que tem lados A e B.
        */
        static void Main(string[] args)
        {
            double a, b, c, triangulo, circulo, trapezio, quadrado, retangulo, pi = 3.14159;

            System.Console.WriteLine("Informe o 1° valor:");
            a = float.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            System.Console.WriteLine("Informe o 2° valor:");
            b = float.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            System.Console.WriteLine("Informe o 3° valor:");
            c = float.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            triangulo = (a * c) / 2;
            circulo = pi * Math.Pow(c, 2.0);
            trapezio = ((a + b) * c) / 2;
            quadrado = Math.Pow(b, 2.0);
            retangulo = a * b;

            System.Console.WriteLine("Triângulo: " + triangulo.ToString("F3", CultureInfo.InvariantCulture));
            Console.WriteLine();
            System.Console.WriteLine("Circulo: " + circulo.ToString("F3", CultureInfo.InvariantCulture));
            Console.WriteLine();
            System.Console.WriteLine("Trapézio: " + trapezio.ToString("F3", CultureInfo.InvariantCulture));
            Console.WriteLine();
            System.Console.WriteLine("Quadrado: " + quadrado.ToString("F3", CultureInfo.InvariantCulture));
            Console.WriteLine();
            System.Console.WriteLine("retângulo: " + retangulo.ToString("F3", CultureInfo.InvariantCulture));
        }
    }
}

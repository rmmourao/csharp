﻿using System;
using System.Globalization;

namespace ExercicioEstruturaCondicional
{
    class Program
    {
        /*
            Você deve fazer um programa que leia um valor qualquer e apresente uma mensagem dizendo em qual dos seguintes intervalos ([0,25], (25,50], (50,75], (75,100]) este valor se encontra. Obviamente se o valor não estiver em nenhum destes intervalos, deverá ser impressa a mensagem “Fora de intervalo”.
        */
        static void Main(string[] args)
        {
            // string intervalo;

            System.Console.Write("Informe um valor:");
            double value = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            if (value < 0 || value > 100)
            {
                System.Console.WriteLine("Fora de intervalo.");
            }
            else if (value <= 25)
            {
                System.Console.WriteLine("[0, 25]");
            }
            else if (value <= 50)
            {
                System.Console.WriteLine("[25, 50]");
            }
            else if (value <= 75)
            {
                System.Console.WriteLine("[50, 75]");
            }
            else
            {
                System.Console.WriteLine("[75, 100]");
            }

            // if (value >= 0 && value <= 25) {
            //     intervalo = "Intervalo [0, 25]";
            // }
            // else if (value > 25 && value <= 50) {
            //     intervalo = "Intervalo [25, 50]";
            // }
            // else if (value > 50 && value <= 75) {
            //     intervalo = "Intervalo [50, 75]";
            // }
            // else if (value > 75 && value <= 100) {
            //     intervalo = "Intervalo [75, 100]";
            // }
            // else {
            //     intervalo = "Fora de intervalo.";
            // }


            // System.Console.WriteLine(intervalo);
        }
    }
}

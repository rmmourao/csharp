﻿using System;

namespace ExercicioMatriz_33
{
    class Program
    /*
        Fazer um programa para ler um número inteiro N e uma matriz de ordem N contendo números inteiros. Em seguida, mostrar a diagonal principal e a quantidade de valores negativos da matriz. 
    */
    {
        static void Main(string[] args)
        {
            Console.Write("Informe o tamanho da matriz: ");
            int n = int.Parse(Console.ReadLine());

            int[,] mat = new int[n, n];

            for (int i = 0; i < n; i++)
            {
                string[] values = Console.ReadLine().Split(' ');

                for (int j = 0; j < n; j++)
                {
                    mat[i, j] = int.Parse(values[j]);
                }
            }

            Console.WriteLine();
            Console.WriteLine("Main diagonal:");
            for (int i = 0; i < n; i++)
            {
                Console.Write(mat[i, i] + " ");
            }

            int count = 0;

            foreach (int item in mat)
            {
                if (item < 0)
                {
                    count++;
                }
            }

            Console.WriteLine();
            Console.WriteLine($"Negative numbers = {count}");
        }
    }
}

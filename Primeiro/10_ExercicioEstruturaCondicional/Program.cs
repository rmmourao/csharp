﻿using System;

namespace ExercicioEstruturaCondicional
{
    class Program
    {
        /*
            Leia a hora inicial e a hora final de um jogo. A seguir calcule a duração do jogo, sabendo que o mesmo pode começar em um dia e terminar em outro, tendo uma duração mínima de 1 hora e máxima de 24 horas.
        */
        static void Main(string[] args)
        {
            System.Console.Write("Informe a hora inicial do jogo:");
            int horaIni = int.Parse(Console.ReadLine());

            System.Console.Write("Informe a hora final do jogo:");
            int horaFin = int.Parse(Console.ReadLine());

            int tempoJogo;

            if (horaIni < horaFin)
            {
                tempoJogo = horaFin - horaIni;
            }
            else
            {
                tempoJogo = 24 - horaIni + horaFin;
            }

            System.Console.WriteLine($"O jogo durou {tempoJogo} hora(s).");

            // if (horaIni > horaFin) {
            //     System.Console.WriteLine($"O jogo durou {(24 - horaIni) + horaFin} hora(s).");
            // }
            // else if (horaIni < horaFin) {
            //     System.Console.WriteLine($"O jogo durou {horaFin - horaIni} hora(s).");
            // }
            // else {
            //     System.Console.WriteLine($"O jogo durou 24 hora(s).");
            // }
        }
    }
}

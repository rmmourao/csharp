﻿using System;
using System.Globalization;

namespace ExercicioEstruturaSequencial
{
    class Program
    {
        /*
            Fazer um programa que leia o número de um funcionário, seu número de horas trabalhadas, o valor que recebe por hora e calcula o salário desse funcionário. A seguir, mostre o número e o salário do funcionário, com duas casas decimais.
        */
        static void Main(string[] args)
        {
            int funcionario, horasTrabalhadas;
            double valorHora, salario;

            System.Console.Write("Insira o número do funcionário:");
            funcionario = int.Parse(Console.ReadLine());

            System.Console.Write("Insira o número de horas trabalhadas:");
            horasTrabalhadas = int.Parse(Console.ReadLine());

            System.Console.Write("Insira o valor da hora trabalhada:");
            valorHora = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            salario = horasTrabalhadas * valorHora;

            Console.WriteLine();
            System.Console.WriteLine("Número: " + funcionario);
            System.Console.WriteLine("Salário: R$ " + salario.ToString("F2", CultureInfo.InvariantCulture));
        }
    }
}

﻿using System;
using System.Globalization;

namespace ExercicioOrientacaoObjetos_28
{
    class Program
    {
        static void Main(string[] args)
        {
            Funcionario f = new Funcionario();

            Console.Write("Informe o nome do funcionário: ");
            f.Nome = Console.ReadLine();

            Console.Write("Informe o salario bruto do funcionário: ");
            f.SalarioBruto = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            Console.Write("Informe o valor do imposto: ");
            f.Imposto = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            Console.WriteLine();
            Console.WriteLine($"Funcionário: {f}");

            Console.WriteLine();
            Console.Write("Informe a porcentagem para aumentar o salário: ");
            double aumento = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            f.AumentarSalario(aumento);

            Console.WriteLine();
            Console.WriteLine($"Dados atualizados: {f}");
        }
    }
}

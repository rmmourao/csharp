﻿using System;

namespace ExercicioEstruturaCondicional
{
    class Program
    {
        /*
            Fazer um programa para ler um número inteiro e dizer se este número é par ou ímpar.
        */
        static void Main(string[] args)
        {
            System.Console.Write("Informe um número inteiro:");
            int n = int.Parse(Console.ReadLine());

            if (n % 2 == 0)
            {
                System.Console.WriteLine("Número par.");
            }
            else
            {
                System.Console.WriteLine("Número ímpar.");
            }
        }
    }
}

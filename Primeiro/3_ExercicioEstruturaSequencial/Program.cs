﻿using System;

namespace ExercicioEstruturaSequencial
{
    class Program
    {
        /*
            Fazer um programa para ler quatro valores inteiros A, B, C e D. A seguir, calcule e mostre a diferença do produto de A e B pelo produto de C e D segundo a fórmula:
                DIFERENCA = (A * B - C * D).
        */
        static void Main(string[] args)
        {
            int a, b, c, d, resultado;

            System.Console.WriteLine("Insira o 1º número inteiro:");
            a = int.Parse(Console.ReadLine());
            System.Console.WriteLine("Insira o 2º número inteiro:");
            b = int.Parse(Console.ReadLine());
            System.Console.WriteLine("Insira o 3º número inteiro:");
            c = int.Parse(Console.ReadLine());
            System.Console.WriteLine("Insira o 4º e último número inteiro:");
            d = int.Parse(Console.ReadLine());

            resultado = (a * b) - (c * d);

            System.Console.WriteLine($"Resultado: {resultado}.");
        }
    }
}

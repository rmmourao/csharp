﻿using System;

namespace NotasTopicosEspeciais_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inferência de tipos: palavra var");
            Console.WriteLine();
            Console.WriteLine("var => Cria uma variável sem informar o tipo, o tipo da variável é inferido através do valor atribuído.");
            Console.WriteLine("A não declaração do tipo de uma variável pode facilitar erros por parte do desenvolvedor.");

            Console.WriteLine();
            var x = 10;
            Console.WriteLine($"var x = {x}; Cria uma variável do tipo int.");
            var y = 20.0;
            Console.WriteLine($"var x = {y}; Cria uma variável do tipo double.");
            var z = "String";
            Console.WriteLine($"var x = {z}; Cria uma variável do tipo string.");

            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine();

            Console.WriteLine("Sintaxe alternativa: switch-case");
            Console.WriteLine();
            Console.WriteLine("Estrutura opcional a vários if-else encadeados, quando a condição envolve o teste do valor de uma variável.");
            Console.WriteLine();
            Console.WriteLine("Sintaxe:");
            Console.WriteLine();
            Console.WriteLine("var minhaVariavel = (...);");
            Console.WriteLine();
            Console.WriteLine("switch (minhaVariavel)");
            Console.WriteLine("{");
            Console.WriteLine("    case 1:");
            Console.WriteLine("        Console.WriteLine(\"Caso 1\");");
            Console.WriteLine("        break;");
            Console.WriteLine("    case 2:");
            Console.WriteLine("        Console.WriteLine(\"Caso 2\");");
            Console.WriteLine("        break;");
            Console.WriteLine("    default:");
            Console.WriteLine("        Console.WriteLine(\"Caso padrão\");");
            Console.WriteLine("        break;");
            Console.WriteLine("}");

            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine();

            Console.WriteLine("Expressão condicional ternária");
            Console.WriteLine();
            Console.WriteLine("Estrutura opcional ao if-else quando se deseja decidir um VALOR com base em uma condição.");
            Console.WriteLine();
            Console.WriteLine("( condição ) ? valor_se_verdadeiro : valor_se_falso");
            Console.WriteLine();
            Console.WriteLine("Exemplos:");
            Console.WriteLine();
            Console.WriteLine("( 2 > 4 ) ? 50 : 80");
            Console.WriteLine("Resultado: 80");
            Console.WriteLine();
            Console.WriteLine("( 10 != 3 ) ? \"Maria\" : \"Alex\"");
            Console.WriteLine("Resultado: \"Maria\"");

            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine();

            Console.WriteLine("Funções interessantes para string");
            Console.WriteLine();

            string original = "abcde FGHIJ ABC abc DEFG ";
            Console.WriteLine("Original: -" + original + "-");
            Console.WriteLine();

            Console.WriteLine("Formatar: ToLower(), ToUpper(), Trim()");
            Console.WriteLine();

            string s1 = original.ToUpper();
            string s2 = original.ToLower();
            string s3 = original.Trim();

            Console.WriteLine("ToUpper: -" + s1 + "-");
            Console.WriteLine("ToLower: -" + s2 + "-");
            Console.WriteLine("Trim: -" + s3 + "-");
            Console.WriteLine();

            Console.WriteLine("Buscar: IndexOf, LastIndexOf");
            Console.WriteLine();

            int n1 = original.IndexOf("bc");
            int n2 = original.LastIndexOf("bc");

            Console.WriteLine("IndexOf('bc'): " + n1);
            Console.WriteLine("LastIndexOf('bc'): " + n2);
            Console.WriteLine();

            Console.WriteLine("Recortar: Substring(inicio), Substring(inicio, tamanho)");
            Console.WriteLine();

            string s4 = original.Substring(3);
            string s5 = original.Substring(3, 5);

            Console.WriteLine("Substring(3): -" + s4 + "-");
            Console.WriteLine("Substring(3, 5): -" + s5 + "-");
            Console.WriteLine();

            Console.WriteLine("Substituir: Replace(char, char), Replace(string, string)");
            Console.WriteLine();

            string s6 = original.Replace('a', 'x');
            string s7 = original.Replace("abc", "xy");

            Console.WriteLine("Replace('a', 'x'): -" + s6 + "-");
            Console.WriteLine("Replace('abc', 'xy'): -" + s7 + "-");
            Console.WriteLine();

            Console.WriteLine("String.IsNullOrEmpty(str), String.IsNullOrWhiteSpace(str)");
            Console.WriteLine();

            bool b1 = string.IsNullOrEmpty(original);
            bool b2 = string.IsNullOrWhiteSpace(original);

            Console.WriteLine("IsNullOrEmpty: " + b1);
            Console.WriteLine("IsNullOrWhiteSpace: " + b2);
            Console.WriteLine();

            Console.WriteLine("Conversão para numero: int x = int.Parse(str), int x = Convert.ToInt32(str)");
            Console.WriteLine();
            Console.WriteLine("Conversão de número: str = x.ToString(), str = x.ToString(\"C\"), str = x.ToString(\"C3\"), str = x.ToString(\"F2\")");

            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine();

            Console.WriteLine("DateTime");
            Console.WriteLine();
            Console.WriteLine("Representa um INSTANTE");
            Console.WriteLine("É um tipo valor (struct)");
            Console.WriteLine();
            Console.WriteLine("Representação interna");
            Console.WriteLine();
            Console.WriteLine("Um objeto DateTime internamente armazena:");
            Console.WriteLine("O número de \"ticks\" (100 nanosegundos) desde a meia noite do dia 1 de janeiro do ano 1 da era comum");
            Console.WriteLine();

            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine();
            
        }
    }
}

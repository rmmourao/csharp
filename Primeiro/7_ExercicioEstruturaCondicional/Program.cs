﻿using System;

namespace ExercicioEstruturaCondicional
{
    class Program
    {
        /*
            Fazer um programa para ler um número inteiro, e depois dizer se este número é negativo ou não.
        */
        static void Main(string[] args)
        {
            System.Console.WriteLine("Informe um número inteiro:");
            int n = int.Parse(Console.ReadLine());

            if (n < 0)
            {
                System.Console.WriteLine("Negativo.");
            }
            else
            {
                System.Console.WriteLine("Não negativo.");
            }
        }
    }
}

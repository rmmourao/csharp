﻿using System;
using System.Collections.Generic;

namespace ExercicioConjunto_35
{
    class Program
    /*
    No portal de cursos online Udemy, cada usuário possui um código único, representado por um número inteiro. Cada instrutor do portal Udemy pode ter vários cursos, sendo que um mesmo aluno pode se matricular em quantos cursos quiser. Assim, o número total de alunos de um instrutor não é simplesmente a soma dos alunos de todos os cursos que ele possui, pois pode haver alunos repetidos em mais de um curso. O instrutor Joaquim possui três cursos A, B e C, e deseja saber seu número total de alunos. Seu programa deve ler os alunos dos cursos A, B e C do instrutor Joaquim, depois mostrar a quantidade total e alunos dele.
    */
    {
        static void Main(string[] args)
        {
            HashSet<int> A = new HashSet<int>();
            HashSet<int> B = new HashSet<int>();
            HashSet<int> C = new HashSet<int>();

            Console.Write("O curso A possui quantos alunos? ");
            int cursoA = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite os códigos dos alunos do curso A:");
            for (int i = 0; i < cursoA; i++)
            {
                A.Add(int.Parse(Console.ReadLine()));
            }

            Console.Write("O curso B possui quantos alunos? ");
            int cursoB = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite os códigos dos alunos do curso B:");
            for (int i = 0; i < cursoB; i++)
            {
                B.Add(int.Parse(Console.ReadLine()));
            }

            Console.Write("O curso C possui quantos alunos? ");
            int cursoC = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite os códigos dos alunos do curso C:");
            for (int i = 0; i < cursoC; i++)
            {
                C.Add(int.Parse(Console.ReadLine()));
            }

            HashSet<int> totalAlunos = new HashSet<int>();

            totalAlunos.UnionWith(A);
            totalAlunos.UnionWith(B);
            totalAlunos.UnionWith(C);

            Console.WriteLine($"Total de alunos: {totalAlunos.Count}");
        }
    }
}

﻿using System;

namespace ExercicioEstruturaCondicional
{
    class Program
    {
        /*
            Leia 2 valores inteiros (A e B). Após, o programa deve mostrar uma mensagem "Sao Multiplos" ou "Nao sao Multiplos", indicando se os valores lidos são múltiplos entre si. Atenção: os números devem poder ser digitados em ordem crescente ou decrescente.
        */
        static void Main(string[] args)
        {
            // int result;

            System.Console.Write("Informe um número inteiro:");
            int n1 = int.Parse(Console.ReadLine());

            System.Console.Write("Informe outro número inteiro:");
            int n2 = int.Parse(Console.ReadLine());

            if (n1 % n2 == 0 || n2 % n1 == 0)
            {
                System.Console.WriteLine("São multiplos!");
            }
            else
            {
                System.Console.WriteLine("Não são multiplos!");
            }

            // if (n1 >= n2) {
            //     result = n1 % n2;
            // }
            // else {
            //     result = n2 % n1;
            // }

            // if (result == 0) {
            //     System.Console.WriteLine("São multiplos!");
            // }
            // else {
            //     System.Console.WriteLine("Não são multiplos!");
            // }
        }
    }
}

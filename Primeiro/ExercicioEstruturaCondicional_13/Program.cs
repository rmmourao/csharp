﻿using System;
using System.Globalization;

namespace ExercicioEstruturaCondicional
{
    class Program
    {
        /*
            Leia 2 valores com uma casa decimal (x e y), que devem representar as coordenadas
            de um ponto em um plano. A seguir, determine qual o quadrante ao qual pertence o ponto, ou se está sobre um dos eixos cartesianos ou na origem (x = y = 0). Se o ponto estiver na origem, escreva a mensagem “Origem”. Se o ponto estiver sobre um dos eixos escreva “Eixo X” ou “Eixo Y”, conforme for a situação.
        */
        static void Main(string[] args)
        {
            System.Console.Write("Informe o 1º valor (x): ");
            double x = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            System.Console.Write("Informe o 2º valor (y): ");
            double y = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            if (x == 0 && y == 0)
            {
                System.Console.WriteLine("Origem.");
            }
            else if (x == 0)
            {
                System.Console.WriteLine("Eixo Y.");
            }
            else if (y == 0)
            {
                System.Console.WriteLine("Eixo X.");
            }
            else if (x > 0 && y > 0)
            {
                System.Console.WriteLine("Q1.");
            }
            else if (x < 0 && y > 0)
            {
                System.Console.WriteLine("Q2.");
            }
            else if (x < 0 && y < 0)
            {
                System.Console.WriteLine("Q3.");
            }
            else
            {
                System.Console.WriteLine("Q4.");
            }
        }
    }
}

﻿using System;

namespace ExercicioEstruturaRepetitiva
{
    class Program
    {
        /*
            Leia um valor inteiro N. Este valor será a quantidade de valores inteiros X que serão lidos em seguida. Mostre quantos destes valores X estão dentro do intervalo [10,20] e quantos estão fora do intervalo, mostrando essas informações conforme exemplo (use a palavra "in" para dentro do intervalo, e "out" para fora do intervalo).
        */
        static void Main(string[] args)
        {
            int value = 0;
            int inside = 0;
            int outside = 0;
            System.Console.Write("Informe um número inteiro: ");
            int x = int.Parse(Console.ReadLine());

            for (int i = 1; i <= x; i++)
            {
                System.Console.Write("Insira um valor inteiro: ");
                value = int.Parse(Console.ReadLine());

                if (value >= 10 && value <= 20)
                {
                    inside++;
                }
                else
                {
                    outside++;
                }
            }

            System.Console.WriteLine($"{inside} in");
            System.Console.WriteLine($"{outside} out");
        }
    }
}

﻿using System;

namespace ExercicioEstruturaRepetitiva
{
    class Program
    {
        /*
            Escreva um programa que repita a leitura de uma senha até que ela seja válida. Para cada leitura de senha incorreta informada, escrever a mensagem "Senha Invalida". Quando a senha for informada corretamente deve ser impressa a mensagem "Acesso Permitido" e o algoritmo encerrado. Considere que a senha correta é o valor 2002.
        */
        static void Main(string[] args)
        {
            System.Console.Write("Informe a senha: ");
            int senha = int.Parse(Console.ReadLine());

            while (senha != 2002)
            {
                System.Console.WriteLine("Senha inválida.");
                System.Console.Write("Informe a senha novamente: ");
                senha = int.Parse(Console.ReadLine());
            }

            System.Console.WriteLine("Acesso permitido.");
        }
    }
}

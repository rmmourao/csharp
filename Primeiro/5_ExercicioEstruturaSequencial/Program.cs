﻿using System;
using System.Globalization;

namespace ExercicioEstruturaSequencial
{
    class Program
    {
        /*
            Fazer um programa para ler o código de uma peça 1, o número de peças 1, o valor unitário de cada peça 1, o código de uma peça 2, o número de peças 2 e o valor unitário de cada peça 2. Calcule e mostre o valor a ser pago.
        */
        static void Main(string[] args)
        {
            int codigoPeca1, qtdPeca1, codigoPeca2, qtdPeca2;
            double valorPeca1, valorPeca2, valorPagar;

            System.Console.WriteLine("Primeira peça, insira código, quantidade e valor unitário:");
            string[] v1 = Console.ReadLine().Split(' ');
            codigoPeca1 = int.Parse(v1[0]);
            qtdPeca1 = int.Parse(v1[1]);
            valorPeca1 = double.Parse(v1[2], CultureInfo.InvariantCulture);

            System.Console.WriteLine("Segunda peça, insira código, quantidade e valor unitário:");
            v1 = Console.ReadLine().Split(' ');
            codigoPeca2 = int.Parse(v1[0]);
            qtdPeca2 = int.Parse(v1[1]);
            valorPeca2 = double.Parse(v1[2], CultureInfo.InvariantCulture);

            valorPagar = (qtdPeca1 * valorPeca1) + (qtdPeca2 * valorPeca2);

            Console.WriteLine();
            System.Console.WriteLine("Valor total a pagar: " + valorPagar.ToString("F2", CultureInfo.InvariantCulture));
        }
    }
}

﻿using System;

namespace ExercicioEstruturaRepetitiva
{
    class Program
    {
        /*
            Um Posto de combustíveis deseja determinar qual de seus produtos tem a preferência de seus clientes. Escreva um algoritmo para ler o tipo de combustível abastecido (codificado da seguinte forma: 1.Álcool 2.Gasolina 3.Diesel 4.Fim). Caso o usuário informe um código inválido (fora da faixa de 1 a 4) deve ser solicitado um novo código (até que seja válido). O programa será encerrado quando o código informado for o número 4. Deve ser escrito a mensagem: "MUITO OBRIGADO" e a quantidade de clientes que abasteceram cada tipo de combustível, conforme exemplo.
        */
        static void Main(string[] args)
        {
            int alcool = 0;
            int gasolina = 0;
            int diesel = 0;

            System.Console.WriteLine("1 - Álcool");
            System.Console.WriteLine("2 - Gasolina");
            System.Console.WriteLine("3 - Diesel");
            System.Console.WriteLine("4 - Fim");
            System.Console.Write("Informe o tipo de combustível abastecido: ");
            int x = int.Parse(Console.ReadLine());

            while (x != 4)
            {
                if (x == 1)
                {
                    alcool = alcool + 1;
                }
                else if (x == 2)
                {
                    gasolina = gasolina + 1;
                }
                else if (x == 3)
                {
                    diesel = diesel + 1;
                }
                else
                {
                    System.Console.WriteLine("Tipo inválido");
                }

                System.Console.Write("Informe um novo tipo de combustível: ");
                x = int.Parse(Console.ReadLine());
            }

            System.Console.WriteLine("Fim. Muito obrigado.");
            System.Console.WriteLine($"1 - Álcool ({alcool})");
            System.Console.WriteLine($"2 - Gasolina ({gasolina})");
            System.Console.WriteLine($"3 - Diesel ({diesel})");
        }
    }
}

﻿namespace ExercicioOOMembrosEstaticos_30
{
    class ConversorDeMoeda
    {
        public static double Iof = 6.00;

        public static double Conversao(double cotacao, double qtd)
        {
            double total = qtd * cotacao;
            return total + total * Iof / 100.00;
        }
    }
}

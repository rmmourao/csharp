﻿using System;
using System.Globalization;

namespace ExercicioEstruturaSequencial
{
    class Program
    {
        /*
            Faça um programa para ler o valor do raio de um círculo, e depois mostrar o valor da área deste círculo com quatro casas decimais.
            Fórmula da área: area = π . raio2
            Considere o valor de π = 3.14159
        */
        static void Main(string[] args)
        {
            double pi = 3.14159;

            System.Console.WriteLine("Insira valor do raio do círculo:");
            double raio = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            double area = pi * Math.Pow(raio, 2.0);

            System.Console.WriteLine("O valor da área do círculo é " + area.ToString("F4", CultureInfo.InvariantCulture));
        }
    }
}

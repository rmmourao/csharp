﻿using System;
using System.Globalization;

namespace ExercicioEstruturaCondicional
{
    class Program
    {
        /*
            Com base na tabela abaixo, escreva um programa que leia o código de um item e a quantidade deste item. A seguir, calcule e mostre o valor da conta a pagar.
            Código | Especificação      | Preço
            1      | Cachorro Quente    | R$ 4.00
            2      | X-Salada           | R$ 4.50
            3      | X-Bacon            | R$ 5.00
            4      | Torrada Simples    | R$ 2.00
            5      | Refrigerante       | R$ 1.50
        */
        static void Main(string[] args)
        {
            int cod, qtd;
            double preco;
            string nome;

            System.Console.Write("Informe o código do produto:");
            cod = int.Parse(Console.ReadLine());

            System.Console.Write("Informe a quantidade do produto:");
            qtd = int.Parse(Console.ReadLine());

            if (cod == 1)
            {
                nome = "Cachorro Quente";
                preco = 4.00;
            }
            else if (cod == 2)
            {
                nome = "X-Salada";
                preco = 4.50;
            }
            else if (cod == 3)
            {
                nome = "X-Bacon";
                preco = 5.00;
            }
            else if (cod == 4)
            {
                nome = "Torrada Simples";
                preco = 2.00;
            }
            else
            {
                nome = "Refrigerante";
                preco = 1.50;
            }

            System.Console.WriteLine("Resumo da compra:");
            System.Console.WriteLine(nome + " - R$ " + preco.ToString("F2", CultureInfo.InvariantCulture) + " - " + qtd + " unidades.");
            System.Console.WriteLine("Valor a pagar: R$ " + (qtd * preco).ToString("F2", CultureInfo.InvariantCulture));
        }
    }
}

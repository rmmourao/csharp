﻿using System;
using System.Globalization;

namespace ExercicioEstruturaRepetitiva
{
    class Program
    {
        /*
            Escreva um programa para ler as coordenadas (X,Y) de uma quantidade indeterminada de pontos no sistema cartesiano. Para cada ponto escrever o quadrante a que ele pertence. O algoritmo será encerrado quando pelo menos uma de duas coordenadas for NULA (nesta situação sem escrever mensagem alguma).
        */
        static void Main(string[] args)
        {
            System.Console.Write("Insira uma coordenada X: ");
            double x = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            System.Console.Write("Insira uma coordenada Y: ");
            double y = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            while (x != 0 && y != 0)
            {
                if (x > 0 && y > 0)
                {
                    Console.WriteLine();
                    System.Console.WriteLine("Primeiro.");
                }
                else if (x < 0 && y > 0)
                {
                    Console.WriteLine();
                    System.Console.WriteLine("Segundo.");
                }
                else if (x < 0 && y < 0)
                {
                    Console.WriteLine();
                    System.Console.WriteLine("Terceiro.");
                }
                else
                {
                    Console.WriteLine();
                    System.Console.WriteLine("Quarto.");
                }

                System.Console.Write("Insira uma nova coordenada X: ");
                x = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

                System.Console.Write("Insira uma nova coordenada Y: ");
                y = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            }
        }
    }
}

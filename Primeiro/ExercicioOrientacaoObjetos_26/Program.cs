﻿using System;
using System.Globalization;

namespace ExercicioOrientacaoObjetos_26
{
    class Program
    {
        static void Main(string[] args)
        {
            Funcionario f1 = new Funcionario();
            Funcionario f2 = new Funcionario();

            System.Console.Write("Informe o nome do 1º funcionário: ");
            f1.Nome = Console.ReadLine();

            System.Console.Write("Informe o salário do 1º funcionário: ");
            f1.Salario = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            System.Console.Write("Informe o nome do 2º funcionário: ");
            f2.Nome = Console.ReadLine();

            System.Console.Write("Informe o salário do 2º funcionário: ");
            f2.Salario = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            double salarioMedio = (f1.Salario + f2.Salario) / 2;

            System.Console.WriteLine("Salário médio: " + salarioMedio.ToString("F2", CultureInfo.InvariantCulture));
        }
    }
}

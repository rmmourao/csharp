﻿using System;
using System.Globalization;

namespace ExercicioOrientacaoObjetos_27
{
    class Program
    {
        static void Main(string[] args)
        {
            Retangulo retangulo = new Retangulo();

            System.Console.Write("Informe a largura do retângulo: ");
            retangulo.Largura = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            System.Console.Write("Informe a altura do retângulo: ");
            retangulo.Altura = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            System.Console.WriteLine("Area: " + retangulo.Area().ToString("F2", CultureInfo.InvariantCulture));
            System.Console.WriteLine("Perimêtro: " + retangulo.Perimetro().ToString("F2", CultureInfo.InvariantCulture));
            System.Console.WriteLine("Diagonal: " + retangulo.Diagonal().ToString("F2", CultureInfo.InvariantCulture));
        }
    }
}

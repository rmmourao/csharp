﻿using System;
using System.Globalization;

namespace ExercicioEstruturaRepetitiva
{
    class Program
    {
        /*
            Leia 1 valor inteiro N, que representa o número de casos de teste que vem a seguir. Cada caso de teste consiste de 3 valores reais, cada um deles com uma casa decimal. Apresente a média ponderada para cada um destes conjuntos de 3 valores, sendo que o primeiro valor tem peso 2, o segundo valor tem peso 3 e o terceiro valor tem peso 5.
        */
        static void Main(string[] args)
        {
            double n1, n2, n3, media = 0;

            System.Console.Write("Informe um número inteiro: ");
            int n = int.Parse(Console.ReadLine());

            for (int i = 1; i <= n; i++)
            {
                System.Console.Write("Informe o 1º valor: ");
                n1 = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

                System.Console.Write("Informe o 2º valor: ");
                n2 = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

                System.Console.Write("Informe o 3º valor: ");
                n3 = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

                media = ((n1 * 2) + (n2 * 3) + (n3 * 5)) / 10;

                System.Console.WriteLine("Média: " + media.ToString("F1", CultureInfo.InvariantCulture));
            }

        }
    }
}
